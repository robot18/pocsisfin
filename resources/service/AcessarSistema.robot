*** Settings ***
Resource        ${EXECDIR}/resources/commons/Mainframe.robot
Resource        ${EXECDIR}/resources/data/AcessarSistemaData.robot

*** Variables ***
${sistema}      MECH
${posicao_x}    11
${posicao_y}    18

*** Keywords ***
Informar Sistema
    Acessar o Sistema       ${sistema}   ${posicao_x}   ${posicao_y}
